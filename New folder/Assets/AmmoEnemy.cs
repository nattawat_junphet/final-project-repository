﻿using UnityEngine;
using System.Collections;

public class AmmoEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 Tle = this.transform.position;
		Tle.y -= 10 * Time.deltaTime;
		this.transform.position = Tle;
	} 
	
	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "Player") {
			PlayerH ouch = other.gameObject.GetComponent <PlayerH> ();
			ouch.Hp--;
			Destroy (this.gameObject);
		} else if (other.gameObject.tag != "Enemy" &&
			other.gameObject.tag != "EnemyBoss") {
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		}  
	}
}
