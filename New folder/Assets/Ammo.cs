﻿using UnityEngine;
using System.Collections;

public class Ammo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 Tle = this.transform.position;
		Tle.y += 15 * Time.deltaTime;
		this.transform.position = Tle;
	} 

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "EnemyBoss") {
			boss hit = other.gameObject.GetComponent <boss> ();
			hit.BossHp--;
			Destroy (this.gameObject);
		} else {
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		}
	}
}
