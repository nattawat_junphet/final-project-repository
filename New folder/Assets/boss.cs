﻿using UnityEngine;
using System.Collections;

public class boss : MonoBehaviour {

	public int BossHp;
	public GameObject Ammoboss;	
	bool left;	
	float distance;	
	float timer;
	
	// Use this for initialization
	void Start () {
		left = false;	
	}
	
	// Update is called once per frame
	void Update () {
		if (BossHp <= 0) {
			Destroy (this.gameObject);
		}

		timer += Time.deltaTime;
		if (timer >= 0.5f) {
			int yay = Random.Range (0, 5);
			if (yay == 0) {
				Vector3 post = this.transform.position;
				post.y -= 0.9f;
				Instantiate (Ammoboss, post, Quaternion.identity);
			}
			timer = 0;
		}
		
		Vector3 pos= this.transform.position;
		if (left == true) {
			pos.x -= 2 * Time.deltaTime;
			distance+= 2 * Time.deltaTime;
			if (distance>= 12) {
				left = false;
				pos.y -= 1;
				distance= 0;
			}
		} else if (left == false) {
			pos.x += 2 * Time.deltaTime;
			distance+= 2 * Time.deltaTime;
			if (distance>= 12) {
				left = true;
				pos.y -= 1;
				distance= 0;
			}
		}
		this.transform.position = pos;
	
	}
}
