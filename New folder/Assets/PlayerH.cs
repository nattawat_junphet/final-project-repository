﻿using UnityEngine;
using System.Collections;

public class PlayerH : MonoBehaviour {

	public int Hp;
	public float Speed;
	public GameObject PlayerBullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Hp <= 0) {
			Destroy (this.gameObject);
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			Vector3 Tle = this.transform.position;
			Tle.x -= Speed * Time.deltaTime;
			this.transform.position = Tle;
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			Vector3 Tle = this.transform.position;
			Tle.x += Speed * Time.deltaTime;
			this.transform.position = Tle;		
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			Vector3 ice = this.transform.position;
			ice.y = -4.3f;
			Instantiate (PlayerBullet,ice,Quaternion.identity);
		}
	}
}
