﻿using UnityEngine;
using System.Collections;

public class RobinHood : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Ray rx0 = new Ray (transform.position, new Vector3 (1, 0, 0));
		Ray rx1 = new Ray (transform.position, new Vector3 (-1, 0, 0));
		Ray ry0 = new Ray (transform.position, new Vector3 (0, 1, 0));
		Ray ry1 = new Ray (transform.position, new Vector3 (0, -1, 0));

		bool hitX0 = false;
		hitX0 = Physics.Raycast (rx0, 2);
		bool hitX1 = false;
		hitX1 = Physics.Raycast (rx1, 2);
		bool hitY0 = false;
		hitY0 = Physics.Raycast (ry0, 2);
		bool hitY1 = false;
		hitY1 = Physics.Raycast (ry1, 2);

		if (hitX0 == true || hitX1 == true || hitY0 == true || hitY1 == true) {
			Debug.Log ("Hit");
		} else {
			Debug.Log ("Miss");
		}
	}
}
