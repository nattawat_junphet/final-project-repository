﻿using UnityEngine;
using System.Collections;

public class Level01 : MonoBehaviour {

	public GameObject Cube01;
	public GameObject Cube02;
	public GameObject Cube03;

	void Start() {
		int n = 0;
		n = Random.Range (0, 3);

		if (n == 0) {
			Destroy (Cube01);
		}
		else if (n == 1) {
			Destroy (Cube02);
		}
		else if (n == 2) {
			Destroy (Cube03);
		}
	}
}
