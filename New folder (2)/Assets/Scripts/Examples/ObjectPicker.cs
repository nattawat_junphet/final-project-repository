﻿using UnityEngine;
using System.Collections;

public class ObjectPicker : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)) {
			Camera cam = this.gameObject.GetComponent<Camera>();

			Vector3 mousePosition = Input.mousePosition;
			Ray r = cam.ScreenPointToRay(mousePosition);
			bool hitSomething = false;
			hitSomething = Physics.Raycast(r, cam.farClipPlane);
			if(hitSomething == true) {
				Debug.Log ("Hit");
			} else {
				Debug.Log ("Miss");
			}
		}
	}
}
