﻿using UnityEngine;
using System.Collections;

public class MoleEmerge : MonoBehaviour {

	public float Timer;

	void Update () {
		Timer += Time.deltaTime;
		Vector3 pos = this.transform.position;

		if (Timer < 0.5f) {
			pos.y += 5 * Time.deltaTime;
		} else if (Timer < 1) {
			pos.y -= 5 * Time.deltaTime;
		} else {
			Timer = 0;
		}

		this.transform.position = pos;
	}
}
