﻿using UnityEngine;
using System.Collections;

public class MoleLevel : MonoBehaviour {

	public float Timer;
	public GameObject[] Markers;
	public GameObject Mole;
	public int Score;
	public int Lives;
	public bool GameOver;

	public GameObject GameCamera;
	
	void Update () {
		if (Lives >= 1) {
			GameOver = false;
			Timer += Time.deltaTime;
			if (Timer >= 1f) {
				int n = Random.Range (0, Markers.Length);
				Vector3 pos = Markers [n].transform.position;
				Mole.transform.position = pos;
				Timer = 0;
			}
			
			if (Input.GetMouseButtonDown (0)) {
				Camera cam = GameCamera.GetComponent<Camera> ();
				
				Vector3 mousePosition = Input.mousePosition;
				Ray r = cam.ScreenPointToRay (mousePosition);
				bool hitSomething = false;
				hitSomething = Physics.Raycast (r, cam.farClipPlane);
				if (hitSomething == true) {
					Score++;
				} else {
					Score--;
					Lives--;
				}
			}
		} else {
			GameOver = true;
			Vector3 pos = transform.position;
			pos.x += 100;
			Mole.transform.position = pos;
		}
	}
}
