﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UIcontroller2 : MonoBehaviour {

	public GameObject TextNumber;
	public GameObject TextLife;
	public GameObject TextGame;
	public GameObject Mole;
	public float Timer;
	
	void Update () {
		Timer += Time.deltaTime;
		Text s = TextNumber.GetComponent<Text> ();
		Text l = TextLife.GetComponent<Text> ();
		MoleLevel all = Mole.GetComponent<MoleLevel> ();
		s.text = all.Score.ToString ();
		l.text = all.Lives.ToString ();
		
		if (all.GameOver == true) {
			Vector3 gpos = TextGame.transform.position;
			gpos.x = 450;
			TextGame.transform.position = gpos;
		}
	}
}
