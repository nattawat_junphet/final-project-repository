﻿using UnityEngine;
using System.Collections;

public class BasketScript : MonoBehaviour {

	public int SpeedBasket;
	public int TotalScore;

	void Update () {
		Vector3 pos = this.transform.position;
		
		if (Input.GetKey(KeyCode.LeftArrow) ||
		    Input.GetKey(KeyCode.A) ||
		    Input.GetMouseButton(0))
		{
			pos.x += Time.deltaTime * SpeedBasket * -1;
		}
		else if (Input.GetKey(KeyCode.RightArrow) ||
		         Input.GetKey(KeyCode.D) ||
		         Input.GetMouseButton(1))
		{
			pos.x += Time.deltaTime * SpeedBasket;
		}

		this.transform.position = pos;
	}
	void OnCollisionEnter (Collision other)
	{
		Item it = other.collider.gameObject.GetComponent<Item> ();

		TotalScore += it.Score;
	}
}
