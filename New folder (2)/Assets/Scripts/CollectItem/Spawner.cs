﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject GoodItemPrefab;
	public GameObject BadItemPrefab;
	public float Timer;

	void Update () {
		Timer += Time.deltaTime;
		if (Timer >= 1.0f) {
			int n = Random.Range (0, 10);
			if (n <= 7) {
				Instantiate (GoodItemPrefab, transform.position, Quaternion.identity);
			} else {
				Instantiate (BadItemPrefab, transform.position, Quaternion.identity);
			}
			Timer = 0;
		}
	}
}
