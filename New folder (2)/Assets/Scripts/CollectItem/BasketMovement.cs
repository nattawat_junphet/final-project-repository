﻿using UnityEngine;
using System.Collections;

public class BasketMovement : MonoBehaviour {

	public int SpeedMovement;
	public float Timer;
	public Vector3 temp;

	void Start () {
		temp = this.transform.position;
	}

	void Update () {
		Vector3 pos = this.transform.position;
		Timer += Time.deltaTime;
		if (Timer < 0.4f) {
			pos.x += SpeedMovement * 1 * Time.deltaTime;
		} else if (Timer < 0.8f) {
			pos.x += SpeedMovement * -1 * Time.deltaTime;
		} else {
			Timer = 0;
			pos = temp;
		}
		this.transform.position = pos;
	}
}
