﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UIcontroller : MonoBehaviour {

	public GameObject TextNumber;
	public GameObject TextLife;
	public GameObject Basket;
	public float Timer;

	void Update () {
		Timer += Time.deltaTime;
		Text s = TextNumber.GetComponent<Text> ();
		BasketScript score = Basket.GetComponent<BasketScript> ();
		s.text = score.TotalScore.ToString();

	}
}
