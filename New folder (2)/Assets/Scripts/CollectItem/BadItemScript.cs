﻿using UnityEngine;
using System.Collections;

public class BadItemScript : MonoBehaviour {

	public GameObject Basket;
	public float Timer;
	
	void Update () {
		Timer += Time.deltaTime;
		if (Timer > 1.6f) {
			Destroy (this.gameObject);
		}
	}
	
	void OnCollisionEnter (Collision Basket)
	{
		Destroy(this.gameObject);
	}
}
